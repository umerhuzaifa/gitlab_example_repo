% The script to animate the human leg with eoskeleton on it. The goal is to
% understand the movement visually for interpreting the moments generated
% on the joints as well as the forces generated in the bowden cables.

% Umer Huzaifa
% January 21, 2021


function animate(filename)
close all
clc

if nargin==0
   anim_tog('data_cable_act_step.mat')  
   %    anim_sep('data_cable_act.mat');
elseif ischar(filename)     
   anim_tog(filename)  
end

end

function anim_tog(filename)

% The script for animating the two systems - desired and actual, together
% in the same plot window

% Umer Huzaifa
% January 21, 2021

% loading the data
    load(filename)

% model parameters in use
[mth1, mth2, ms1, ms2, m_thigh, m_shank, thigh, shank,...
    thigh_com, shank_com, I_thigh_com, I_shank_com,...
    Lth, lth, Lsh, D_thf, D_thb, D_shf, D_shb, d_thf, d_thb, d_shf, d_shb, g]= model_params_leg;

    % initializing the geometric objects for simulated data

    q1is = x_model(1, 1); q2is = x_model(1, 2);
    p_hip_s = [0; 0];
    [p_foot_s, p_kn_s] = shank_end([q1is; q2is]');
    p_kn_s = rot2(q1is) * [0; -thigh]; % [sin(q1); -cos(q1)];
    p_foot_s = p_kn_s + rot2(q1is-q2is) * [0; -shank]; 
        
    figure
    
    % Links initialized for the simulated data
    [link1s, link2s] = init_links(p_hip_s, p_kn_s, p_foot_s, 'sim');
    
    load('right_kinem.mat');
    
    t_norm  = t_model./max(t_model);
    x_des = pi/180 *[hip_interp(t_norm), knee_interp(t_norm), vhip_interp(t_norm), vknee_interp(t_norm)];
    
    q1i = x_des(1, 1); q2i = x_des(1, 2);
    p_hip = [0; 0];    
    [p_foot, p_kn] = shank_end([q1i; q2i]');
    p_kn = rot2(q1i) * [0; -thigh]; % [sin(q1); -cos(q1)];
    p_foot = p_kn + rot2(q1i-q2i) * [0; -shank]; 

    % Links initialized for the desired data
    [link1, link2] = init_links(p_hip, p_kn, p_foot, 'des');
    ground = line([-0.5, 0.9], [-(thigh+shank), -(thigh+shank)],...
    'LineWidth', 4, 'Color', zeros(1,3));

    title(['\fontsize{16}Comparison of {\color{red}Simulated\color{black} and \color{green}Desired} Animation'],'interpreter','tex')
    txt = sprintf('Gait Percent =  %06.2f', 00.00);
    gait_pc_display = text(-0.4, -0.1, txt);
    for i=1:size(t_model, 1)    
        [p_foot_s, p_kn_s] = shank_end(x_model(i, :));
        set(link1s, 'XData', [0, p_kn_s(1)], 'YData', [0, p_kn_s(2)])
        set(link2s, 'XData', [p_kn_s(1), p_foot_s(1)], 'YData', [p_kn_s(2), p_foot_s(2)])

        [p_foot, p_kn] = shank_end(x_des(i, :));
        set(link1, 'XData', [0, p_kn(1)], 'YData', [0, p_kn(2)]);
        set(link2, 'XData', [p_kn(1), p_foot(1)], 'YData', [p_kn(2), p_foot(2)]);    

        hold on
        plot(p_foot_s(1), p_foot_s(2),'r+')
        plot(p_foot(1), p_foot(2),'go')        
        gait_pc_display.String(end-5:end)=sprintf('%06.2f',t_model(i)*100/t_model(end)); 
        drawnow;
%         pause(0.1)
    end
    
 
end


% For separate animation of simulated and desired kinematics
function anim_sep(filename)
% loading the data
load(filename)

% model parameters in use
[mth1, mth2, ms1, ms2, m_thigh, m_shank, thigh,...
    shank, Lth, lth, Lsh, D_thf, D_thb, D_shf, D_shb,...
    d_thf, d_thb, d_shf, d_shb, g] = model_params_leg;

% initializing the geometric objects

    q1i = x_model(1, 1); q2i = x_model(1, 2);
    p_hip = [0; 0];
    [p_foot, p_kn] = shank_end([q1i; q2i]')
    p_kn = rot2(q1i) * [0; -thigh]; % [sin(q1); -cos(q1)];
    p_foot = p_kn + rot2(q1i-q2i) * [0; -shank]; 
    
    % defining the geometric objects
    figure
    subplot(121)
    [link1, link2] = init_links(p_hip, p_kn, p_foot, 'sim');
% Simulated trajectory from the model
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 title('Simulated Trajectory from the Model')
for i=1:size(t_model, 1)
    
    [p_foot, p_kn] = shank_end(x_model(i, :));
    set(link1, 'XData', [0, p_kn(1)], 'YData', [0, p_kn(2)])
    set(link2, 'XData', [p_kn(1), p_foot(1)], 'YData', [p_kn(2), p_foot(2)])
    drawnow;
end

 % Desired trajectory from dataset
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 load('right_kinem.mat');
 t_norm  = t_model./max(t_model);
 x_des = pi/180 *[hip_interp(t_norm), knee_interp(t_norm), vhip_interp(t_norm), vknee_interp(t_norm)];
 q1i = x_des(1, 1); q2i = x_des(1, 2);
 p_hip = [0; 0];
 [p_foot, p_kn] = shank_end([q1i; q2i]')
 p_kn = rot2(q1i) * [0; -thigh]; % [sin(q1); -cos(q1)];
 p_foot = p_kn + rot2(q1i-q2i) * [0; -shank]; 
 subplot(122)
 
 title('Desired Trajectory from the Dataset')
 [link1, link2] = init_links(p_hip, p_kn, p_foot, 'des');
 
 
 for i=1:size(t_model, 1)
    
    [p_foot, p_kn] = shank_end(x_des(i, :));
    set(link1, 'XData', [0, p_kn(1)], 'YData', [0, p_kn(2)]);
    set(link2, 'XData', [p_kn(1), p_foot(1)], 'YData', [p_kn(2), p_foot(2)]);
    drawnow;
end

 
end

function [link1, link2] = init_links(p_hip, p_kn, p_foot, flag)


    link1 = line([p_hip(1), p_kn(1)],[p_hip(2), p_kn(2)]);
    link2 = line([p_kn(1), p_foot(1)],[p_kn(2), p_foot(2)]);
    set(link1,'LineStyle', '--', 'LineWidth', 2, 'Color', zeros(1,3))
    set(link2, 'LineStyle', '--','LineWidth', 2, 'Color', zeros(1,3))

    link1 = line([p_hip(1), p_kn(1)],[p_hip(2), p_kn(2)]);
    link2 = line([p_kn(1), p_foot(1)],[p_kn(2), p_foot(2)]);    
    
    axis([-0.6, 0.5, -1.0, 0])

    if (flag == 'sim')        
        set(link1, 'LineWidth', 2, 'Color', [1, 0, 0])
        set(link2, 'LineWidth', 2, 'Color', [1, 0, 0])
    else
        set(link1, 'LineWidth', 2, 'Color', [0, 1, 0])
        set(link2, 'LineWidth', 2, 'Color', [0, 1, 0])        
    end
    
end


function [p_foot, p_kn] = shank_end(x)
% finds the foot position of the leg+exo system
p_foot = [];

[mth1, mth2, ms1, ms2, m_thigh, m_shank, thigh,...
    shank, Lth, lth, Lsh, D_thf, D_thb, D_shf, D_shb,...
    d_thf, d_thb, d_shf, d_shb, g] = model_params_leg;

   for i=1:size(x, 1)
    q1 = x(i, 1); q2 = x(i, 2);
    p_kn = rot2(q1) * [0; -thigh]; % [sin(q1); -cos(q1)];
    p_foot = p_kn + rot2(q1-q2) * [0; -shank]; 

   end

end
